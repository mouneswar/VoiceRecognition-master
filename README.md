# Voice recognition app

Created by Edward Curtis https://www.freelancer.com/u/curtiscode for https://www.freelancer.com/u/Mouneshh

## Build with maven
First you must build https://github.com/amaurycrickx/recognito with maven
Then a runnable jar can be created using `mvn clean package assembly:single`

## Usage

Run the app by running the main class VoiceRecognition.
The two options presented are to record new voices for valid users, or to verify that a voice matches an existing valid user.
A voice will count as a match if the closest match to any of the voices is one within the USER_VOICES_DIRECTORY or one added by a user, and the likelihood percentage is equal to or greater than the value of the LIKELIHOOD_PERCENTAGE constant.
Note that voice recognition is more reliable with more samples, so you may have to add a few voices for a person to get a perfect match.

## Configuration

There are multiple configurable constants in the program, which all lie in the Constants class.

RECORDING_DIRECTORY - The directory where recordings made by the program are stored.
SAMPLE_VOICES_DIRECTORY - This directory contains subdirectories of voices. These help the recognito algorithm determine a baseline and give more accurate results.
USER_VOICES_DIRECTORY - This directory contains subdirectories of voices that have been permanently added as valid users, to save recording every time and also to allow us to add more than one voice sample per person.
LIKELIHOOD_PERCENTAGE - The minimum likelihood percentage required for a voice match. If you have a lot of sample voices this can be very high (99 or 100), but if you are only using a few sample voices this will need to be lower.

## Credits

Voice recognition taken from https://github.com/amaurycrickx/recognito and shared under apache 2.0 licence.
Voice samples taken from http://www.repository.voxforge1.org/downloads/SpeechCorpus/Trunk/Audio/Main/16kHz_16bit/ and shared under the GPL licence.