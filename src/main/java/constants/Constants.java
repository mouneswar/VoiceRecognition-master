package constants;

import java.io.File;

public class Constants {

    // UI
    public static final String APP_STARTING_LABEL = "App starting... Please wait while content loads.";
    public static final String RECORDING_LABEL = "Recording...";
    public static final String PROCESSING_LABEL = "Processing...";
    public static final String READY_LABEL = "Ready to record...";
    public static final String VOICE_MATCHES = "Voice recording matched: ";
    public static final String VOICE_NO_MATCH = "Voice recording not matched.";

    // File names
    public static final String RECORDING_DIRECTORY = System.getProperty("user.home") + File.separator + "recordings";
    public static final String SAMPLE_VOICES_DIRECTORY = "src/main/resources/sampleVoices";
    public static final String USER_VOICES_DIRECTORY = "src/main/resources/validVoices";
    public static final String VOICE_PREFIX = "voice_";
    public static final String VERIFY_FILENAME = "voiceToVerify";
    public static final int LIKELIHOOD_PERCENTAGE = 99;
}
