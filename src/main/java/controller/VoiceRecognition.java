package controller;

import buttons.AddVoiceButton;
import buttons.StopButton;
import buttons.VerifyVoiceButton;
import constants.Constants;
import voicerecognition.Voices;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static constants.Constants.RECORDING_DIRECTORY;

public class VoiceRecognition {

    private JFrame frame;
    private JLabel label;
    private JButton btnAddVoice;
    private JButton btnVerifyVoice;
    private JButton btnStopRecording;
    private AudioRecorder audioRecorder;
    private boolean lastRecordingIsVerify = false;
    private String lastRecordingName;

    public static void main(String[] args) {

        VoiceRecognition vr = new VoiceRecognition();
        javax.swing.SwingUtilities.invokeLater(() -> {
            vr.createAndShowGUI();
            // Initialise the Voices service in a separate thread because it takes a little bit of time to load
            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.submit(() -> vr.initVoices());
        });
    }

    private void initVoices() {
        Voices.instance();
        label.setText(Constants.RECORDING_LABEL);
        btnAddVoice.setEnabled(true);
        btnVerifyVoice.setEnabled(true);
    }

    public void startRecording(String fileName, boolean lastRecordingIsVerify) {
        label.setText(Constants.RECORDING_LABEL);
        btnVerifyVoice.setEnabled(false);
        btnAddVoice.setEnabled(false);
        btnStopRecording.setEnabled(true);
        audioRecorder = new AudioRecorder(fileName);
        audioRecorder.startRecording();
        this.lastRecordingIsVerify = lastRecordingIsVerify;
        lastRecordingName = fileName;
    }

    public void stopRecording() {
        btnVerifyVoice.setEnabled(true);
        btnAddVoice.setEnabled(true);
        btnStopRecording.setEnabled(false);
        if (audioRecorder != null) {
            audioRecorder.stopRecording();
            if (lastRecordingIsVerify) {
                // Compare voice file with existing recordings
                label.setText(Constants.PROCESSING_LABEL);
                // Verify the voice in a new thread
                ExecutorService executor = Executors.newSingleThreadExecutor();
                executor.submit(() -> verifyVoice());
            } else {
                String fileName = RECORDING_DIRECTORY + File.separator + lastRecordingName + ".wav";
                Voices.instance().addVoice(lastRecordingName, new File(fileName), true);
                label.setText(Constants.READY_LABEL);
            }
        }
    }

    private void verifyVoice() {
        File voice = new File(RECORDING_DIRECTORY + File.separator + Constants.VERIFY_FILENAME + ".wav");
        String match = Voices.instance().getMatch(voice);
        if(match != null) {
            label.setText(Constants.VOICE_MATCHES + match);
        } else {
            label.setText(Constants.VOICE_NO_MATCH);
        }
    }

    private void createAndShowGUI() {
        frame = new JFrame("VoiceRecognition");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        label = new JLabel(Constants.APP_STARTING_LABEL);
        btnStopRecording = new StopButton("Stop recording", this);
        btnAddVoice = new AddVoiceButton("Add new voice", this);
        btnVerifyVoice = new VerifyVoiceButton("Verify voice", this);
        btnStopRecording.setEnabled(false);
        btnAddVoice.setEnabled(false);
        btnVerifyVoice.setEnabled(false);

        Container pane = frame.getContentPane();
        GroupLayout gl = new GroupLayout(pane);

        gl.setHorizontalGroup(gl.createParallelGroup()
                .addComponent(label)
                .addComponent(btnAddVoice)
                .addComponent(btnVerifyVoice)
                .addComponent(btnStopRecording)
        );

        gl.setVerticalGroup(gl.createSequentialGroup()
                .addComponent(label)
                .addGap(10)
                .addComponent(btnAddVoice)
                .addComponent(btnVerifyVoice)
                .addComponent(btnStopRecording)
        );

        pane.setLayout(gl);

        frame.pack();
        frame.setVisible(true);
    }
}
