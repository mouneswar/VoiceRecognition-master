package controller;

import constants.Constants;

import javax.sound.sampled.*;
import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AudioRecorder {

    private final TargetDataLine line;
    private final File wavFile;
    private AudioFileFormat.Type fileType = AudioFileFormat.Type.WAVE;

    public AudioRecorder(String fileName) {
        try {
            File file = new File(Constants.RECORDING_DIRECTORY);
            System.out.println("Saving recordings in directory: " + file.toString());
            if (!file.exists()) {
                file.mkdirs();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        wavFile = new File(Constants.RECORDING_DIRECTORY + File.separator + fileName + ".wav");
        AudioFormat audioFormat = getAudioFormat();
        DataLine.Info info = new DataLine.Info(TargetDataLine.class, audioFormat);
        if (!AudioSystem.isLineSupported(info)) {
            throw new RuntimeException("Audio line not supported. Check microphone configuration.");
        }
        try {
            line = (TargetDataLine) AudioSystem.getLine(info);
            line.open(audioFormat);
        } catch (LineUnavailableException ex) {
            throw new RuntimeException("Line unavailable", ex);
        }
    }

    public void startRecording() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            try {
                AudioFormat format = getAudioFormat();
                DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);

                line.open(format);
                line.start();

                AudioInputStream ais = new AudioInputStream(line);

                // start recording
                AudioSystem.write(ais, fileType, wavFile);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    public void stopRecording() {
        line.stop();
        line.close();
    }

    /**
     * Defines an audio format
     */
    private AudioFormat getAudioFormat() {
        float sampleRate = 16000;
        int sampleSizeInBits = 8;
        int channels = 2;
        boolean signed = true;
        boolean bigEndian = true;
        AudioFormat format = new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
        return format;
    }
}
