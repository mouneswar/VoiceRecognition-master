package voicerecognition;

import com.bitsinharmony.recognito.MatchResult;
import com.bitsinharmony.recognito.Recognito;
import constants.Constants;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Voices {

    private static final Voices INSTANCE = new Voices();

    private final Set<String> USER_ADDED;

    private final Recognito<String> RECOGNITO = new Recognito<>(16000.0f);

    public static Voices instance() {
        return INSTANCE;
    }

    private Voices() {
        USER_ADDED = new HashSet<>();
        // Add voices for users
        addVoices(Constants.USER_VOICES_DIRECTORY, true);
        // Add the sample voices to give the algorithm a baseline to work against
        addVoices(Constants.SAMPLE_VOICES_DIRECTORY, false);
        System.out.println("All voices processed");
    }

    private void addVoices(String directory, boolean userAdded) {
        File sampleVoices = new File(directory);
        if (sampleVoices != null && sampleVoices.isDirectory()) {
            for (String subDirectory : sampleVoices.list()) {
                System.out.println("Processing: " + directory + subDirectory);
                try {
                    Files.walk(Paths.get(directory + File.separator + subDirectory))
                            .filter(Files::isRegularFile)
                            .filter(s -> s.toString().endsWith(".wav"))
                            .forEach(s -> addVoice(subDirectory, s.toFile(), userAdded));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            throw new RuntimeException("Directory not found: " + directory);
        }
    }

    public void addVoice(String key, File voice, boolean userAdded) {
        try {
            try {
                RECOGNITO.mergeVoiceSample(key, voice);
            } catch (IllegalArgumentException e) {
                RECOGNITO.createVoicePrint(key, voice);
            } finally {
                if (userAdded) {
                    USER_ADDED.add(key);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getMatch(File voice) {
        try {
            List<MatchResult<String>> matches = RECOGNITO.identify(voice);
            MatchResult<String> match = matches.get(0);
            if  (match != null && match.getLikelihoodRatio() >= Constants.LIKELIHOOD_PERCENTAGE && USER_ADDED.contains(match.getKey())) {
                return match.getKey();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
