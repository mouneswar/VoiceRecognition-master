package buttons;

import controller.VoiceRecognition;

import javax.swing.*;
import java.awt.event.MouseEvent;

public class StopButton extends JButton {

    private VoiceRecognition voiceRecognition;

    public StopButton(String text, VoiceRecognition voiceRecognition) {
        super(text);
        this.voiceRecognition = voiceRecognition;
    }

    @Override
    protected void processMouseEvent(MouseEvent e) {
        if (e.getID() == MouseEvent.MOUSE_CLICKED) {
            voiceRecognition.stopRecording();
        }
        super.processMouseEvent(e);
    }
}
