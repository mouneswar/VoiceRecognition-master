package buttons;

import controller.VoiceRecognition;

import javax.swing.*;
import java.awt.event.MouseEvent;

public abstract class RecordingButton extends JButton {

    private VoiceRecognition voiceRecognition;

    public RecordingButton(String text, VoiceRecognition voiceRecognition) {
        super(text);
        this.voiceRecognition = voiceRecognition;
    }

    @Override
    protected void processMouseEvent(MouseEvent e) {
        if (e.getID() == MouseEvent.MOUSE_CLICKED) {
            System.out.println("About to record");
            voiceRecognition.startRecording(getFileName(), this instanceof VerifyVoiceButton);
        }
        super.processMouseEvent(e);
    }

    public abstract String getFileName();
}
