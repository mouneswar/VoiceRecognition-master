package buttons;

import constants.Constants;
import controller.VoiceRecognition;

public class AddVoiceButton extends RecordingButton {

    public AddVoiceButton(String text, VoiceRecognition voiceRecognition) {
        super(text, voiceRecognition);
    }

    @Override
    public String getFileName() {
        return Constants.VOICE_PREFIX + Long.toString(System.currentTimeMillis());
    }
}
