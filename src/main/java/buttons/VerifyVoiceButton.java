package buttons;

import constants.Constants;
import controller.VoiceRecognition;

public class VerifyVoiceButton extends RecordingButton {

    public VerifyVoiceButton(String text, VoiceRecognition voiceRecognition) {
        super(text, voiceRecognition);
    }

    @Override
    public String getFileName() {
        return Constants.VERIFY_FILENAME;
    }
}
